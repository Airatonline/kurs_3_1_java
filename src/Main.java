
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        File[] input;
        FileFilter imageFilter = new FileNameExtensionFilter(
                "Image files", ImageIO.getReaderFileSuffixes());
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        JFileChooser fileopen = new JFileChooser();
        fileopen.setFileFilter(imageFilter);
        fileopen.setMultiSelectionEnabled(true);
        int ret = fileopen.showDialog(null, "Открыть файл");

        if (ret == JFileChooser.APPROVE_OPTION) {
            input = fileopen.getSelectedFiles();
            for (File file : input) {
                Image image = ImageIO.read(file);
                JLabel iblimage = new JLabel(new ImageIcon(image.getScaledInstance(1000, 500, Image.SCALE_SMOOTH)));
                JFrame frame = new JFrame();
                frame.setTitle(file.getName());
                frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);


                frame.getContentPane().add(iblimage, BorderLayout.CENTER);
                frame.setSize(1000, 500);

                frame.setVisible(true);
                int red = 0, green = 0, blue = 0;
                System.out.println(file.exists());
                System.out.println(file.getAbsolutePath());

                Mat img = Imgcodecs.imread(file.getPath());
                for (int j = 0; j < img.rows(); j++) {
                    for (int t = 0; t < img.cols(); t++) {
                        double[] pixel = img.get(j, t);
                        if (pixel[0] > pixel[1] && pixel[0] > pixel[2]) {
                            blue++;
                        } else if (pixel[1] > pixel[0] && pixel[1] > pixel[2]) {
                            green++;
                        } else if (pixel[2] > pixel[0] && pixel[2] > pixel[1]) {
                            red++;
                        }

                    }
                }
                System.out.println(red + "  " + green + "    " + blue + " " + "    " + String.format("%.2f", persentOfRed(red, green, blue)) + "  " + String.format("%.2f", persentOfGreen(red, green, blue)) + "    " + String.format("%.2f", persentOfBlue(red, green, blue)));

                if (isAutumn(red, green, blue)) System.out.println("\nНа изображении представлена осень\n");
                else if (isWinter(red, green, blue)) System.out.println("\nНа изображении представлена зима\n");
                else if (isSummer(red, green, blue)) System.out.println("\nНа изображении представлена лето\n");
                else System.out.println("\nНа изображении представлена весна\n");

            }
        }

    }

    static boolean isWinter(int red, int green, int blue) {    //зима

        if (persentOfBlue(red, green, blue) > 0.5 && persentOfGreen(red, green, blue) < 0.43)
            return true;
        return false;
    }

    static boolean isSpring(int red, int green, int blue) {    //весна
        if (!isWinter(red, green, blue) && !isAutumn(red, green, blue) && !isSummer(red, green, blue))
            return true;
        return false;
    }

    static boolean isAutumn(int red, int green, int blue) {//осень
        if (persentOfRed(red, green, blue) > 0.52)
            return true;
        return false;
    }

    static boolean isSummer(int red, int green, int blue) {//лето
        if (persentOfGreen(red, green, blue) > 0.43)
            return true;
        return false;
    }

    static double persentOfRed(int red, int green, int blue) {
        return ((double) red / (red + green + blue));
    }

    static double persentOfGreen(int red, int green, int blue) {
        return ((double) (green) / (red + green + blue));
    }

    static double persentOfBlue(int red, int green, int blue) {
        return ((double) (blue) / (red + green + blue));
    }

}

